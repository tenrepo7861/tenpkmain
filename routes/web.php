<?php

use App\Http\Controllers\Tenacademy\AdminCourseController;
use App\Http\Controllers\Tenacademy\OverviewController;
use App\Http\Controllers\Tenacademy\RoleController;
use App\Http\Controllers\Tenacademy\UserController;
use App\Http\Controllers\Tenacademy\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => ['role:admin']], function () {
// Auth::routes();
//route for AdminCourseController
Route::group(
    ['prefix' => 'admin'],function () {
        Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('admin.dashboard');
        Route::get('/show', [AdminCourseController::class, 'index'])->name('admin.course.show');
        Route::get('/create', [AdminCourseController::class, 'create'])->name('admin.course.create');
        Route::post('/store', [AdminCourseController::class, 'store'])->name('admin.course.store');
    }
);
//route for AdminCourseController
Route::group(
    ['prefix' => 'admin'],function () {
        Route::get('overview/show', [OverviewController::class, 'index'])->name('admin.overview.show');
        Route::get('overview/create', [OverviewController::class, 'create'])->name('admin.overview.create');
        Route::post('overview/store', [OverviewController::class, 'store'])->name('admin.overview.store');
    }
);





// Route::group(['prefix' => 'course'], function () {
//     Route::get('/show', [AdminCourseController::class, 'index']);
// });

    Route::group(
        ['prefix' => 'admin'],
        function () {
            Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('admin.dashboard');
            Route::get('/show', [AdminCourseController::class, 'index'])->name('admin.course.show');
            Route::get('/create', [AdminCourseController::class, 'create'])->name('admin.course.create');
            Route::post('/store', [AdminCourseController::class, 'store'])->name('admin.course.store');
        }
    );
});


Route::group(['middleware' => ['role:admin']], function () {
    Route::resource('/roles', RoleController::class);
    Route::resource('/users', UserController::class);
    Route::post('/asignRole', [UserController::class, 'assignRole'])->name('assignRole');
});

Route::group(['middleware' => ['role:user']], function () {

    Route::group(
        ['prefix' => 'user'],
        function () {
            Route::get('/dashboard', [UsersController::class, 'index'])->name('User.dashboard');
        }
    );
});



Route::get('/pages/tentech', function () {
    include('tentech.php');
});
Route::get('/pages/tenacademy', function () {
    include('tenacademy.php');
});
