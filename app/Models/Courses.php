<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'description',
        'price',
        'image',
        'duration',
        'created_at',
        'created_at',
        'description_include'
    ];

    public function overviews()
    {
        return  $this->belongsTo(Overview::class,'course_id', 'id');
    }

}
