<?php

namespace App\Http\Controllers\Tenacademy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasRole('user')) {
            return view('admin.dashboard');
        }
    }
}