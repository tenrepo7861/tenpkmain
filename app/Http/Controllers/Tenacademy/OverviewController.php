<?php

namespace App\Http\Controllers\Tenacademy;

use App\Http\Controllers\Controller;
use App\Models\Courses;
use App\Models\Overview;
use Illuminate\Http\Request;

class OverviewController extends Controller
{
    public function index()
    {
        $overviews = Overview::orderby('id', 'desc')->with('course')->get();
        //return $overviews[0]->course->title;
        $courses = Courses::all();
        return view('admin.overview.index', compact('overviews'));
    }
    public function create()
    {
        $courses = Courses::all();
        return view('admin.overview.create', compact('courses'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'course_id' => 'required',
        ], [
            'course_id.required' => 'This field is required *'

        ]);
        $course_id = $request->course_id;
        $learns = array_filter($request->learn);
        $requirements = $request->requirements;
        //remove null value from array using array_filter function
        $remove_null_value = array_filter($requirements);
        foreach ($learns as $key => $learn) {
            $addOverview = new Overview();
            $addOverview->course_id = $course_id;
            $addOverview->learn = $learn;
            $addOverview->requirements = $remove_null_value[$key];
            $addOverview->save();
        }
        return redirect('admin/overview/show')->with('success', 'Overview Added!');
    }
}