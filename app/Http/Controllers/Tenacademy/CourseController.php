<?php

namespace App\Http\Controllers\Tenacademy;

use App\Http\Controllers\Controller;
use App\Models\Courses;
use App\Models\Overview;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function grid()
    {
        return view('pages.tenacademy.course.course');
    }

    public function list()
    {
        return view('pages.tenacademy.course.course-list');
    }

    public function topic()
    {
        return view('pages.tenacademy.course.course-path');
    }

    public function lesson()
    {
        return view('pages.tenacademy.course.course-lesson');
    }
    public function detail($id)
    {

        $course = Courses::where('id', $id)->get();
        foreach ($course as $row) {
            $course_id = $row->id;
            $price = $row->price;
            $title = $row->title;
            $duration = $row->duration;
            $description_include = $row->description_include;
            $about_course = $row->about_course;

            // $price = $this->price;
            // $course_id = $this->course_id;
            // $title = $this->title;
            // $duration = $this->duration;
            // $description_include = $this->description_include;
            // $about_course = $this->about_course;
        }
        $overviews = Overview::where('course_id', $id)->get();
        // return $about_course;
        return view('pages.tenacademy.course.course-detail', compact('price', 'duration', 'title', 'course_id', 'description_include', 'overviews', 'about_course'));
    }
}