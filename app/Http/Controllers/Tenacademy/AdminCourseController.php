<?php

namespace App\Http\Controllers\Tenacademy;

use App\Http\Controllers\Controller;
use App\Models\Courses;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminCourseController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect('pages/tenacademy/login');
        }
        $courses = Courses::orderBy('id', 'DESC')->paginate(10);
        return view('admin.course.index', compact('courses'));
    }

    public function create()
    {
        if (!Auth::check()) {
            return redirect('pages/tenacademy/login');
        }
        return view('admin.course.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'description' => 'required',
            'duration' => 'required',
            'duration' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:500000',
        ], [
            'title.required' => 'This field is required *',
            'description.required' => 'This field is required *',
            'price.required' => 'This field is required *',
            'duration.required' => 'This field is required *',
            'image.required' => 'This field is required *'

        ]);

        $addCourse = new Courses();
        $addCourse->title = $request->title;
        $addCourse->description = $request->description;
        $addCourse->price = $request->price;
        $addCourse->duration = $request->duration;
        $addCourse->about_course = $request->about_course;
        $addCourse->description_include = json_encode($request->description_include);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalName();
            $filename = time() . '.' . $extension;
            $file->move('courseImages', $filename);
            $addCourse->image = $filename;
        }

        $addCourse->save();
        //Alert::success('Success', 'Package Added!');
        return redirect('/admin/show')->with('success', 'Course Added!');
    }
    public function dashboard()
    {
        if (auth()->user()->hasRole('admin')) {

            return view('admin.dashboard');
        }
        // else {
        //     return view('pages.tenacademy.frontPages.front');
        // }
    }
    public function user()
    {
        if (auth()->user()->hasRole('admin')) {
            $user = User::all();

            return view('admin.users.users', compact('user'));
        }
    }
}
