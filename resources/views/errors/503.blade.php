<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from colorlib.com/etc/cs/comingsoon_04/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Sep 2022 10:58:59 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <title>The Educational Network</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="error/images/icons/favicon.ico" />

    <link rel="stylesheet" type="text/css" href="error/vendor/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="error/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="error/fonts/iconic/css/material-design-iconic-font.min.css">

    <link rel="stylesheet" type="text/css" href="error/vendor/animate/animate.css">

    <link rel="stylesheet" type="text/css" href="error/vendor/select2/select2.min.css">

    <link rel="stylesheet" type="text/css" href="error/css/util.css">
    <link rel="stylesheet" type="text/css" href="error/css/main.css">

    <meta name="robots" content="noindex, follow">
    <script nonce="1efe75e9-11f2-42dc-a8d8-938ccd9da0de">
        (function(w, d) {
            ! function(a, e, t, r) {
                a.zarazData = a.zarazData || {};
                a.zarazData.executed = [];
                a.zaraz = {
                    deferred: []
                };
                a.zaraz.q = [];
                a.zaraz._f = function(e) {
                    return function() {
                        var t = Array.prototype.slice.call(arguments);
                        a.zaraz.q.push({
                            m: e,
                            a: t
                        })
                    }
                };
                for (const e of ["track", "set", "ecommerce", "debug"]) a.zaraz[e] = a.zaraz._f(e);
                a.zaraz.init = () => {
                    var t = e.getElementsByTagName(r)[0],
                        z = e.createElement(r),
                        n = e.getElementsByTagName("title")[0];
                    n && (a.zarazData.t = e.getElementsByTagName("title")[0].text);
                    a.zarazData.x = Math.random();
                    a.zarazData.w = a.screen.width;
                    a.zarazData.h = a.screen.height;
                    a.zarazData.j = a.innerHeight;
                    a.zarazData.e = a.innerWidth;
                    a.zarazData.l = a.location.href;
                    a.zarazData.r = e.referrer;
                    a.zarazData.k = a.screen.colorDepth;
                    a.zarazData.n = e.characterSet;
                    a.zarazData.o = (new Date).getTimezoneOffset();
                    a.zarazData.q = [];
                    for (; a.zaraz.q.length;) {
                        const e = a.zaraz.q.shift();
                        a.zarazData.q.push(e)
                    }
                    z.defer = !0;
                    for (const e of [localStorage, sessionStorage]) Object.keys(e || {}).filter((a => a.startsWith(
                        "_zaraz_"))).forEach((t => {
                        try {
                            a.zarazData["z_" + t.slice(7)] = JSON.parse(e.getItem(t))
                        } catch {
                            a.zarazData["z_" + t.slice(7)] = e.getItem(t)
                        }
                    }));
                    z.referrerPolicy = "origin";
                    z.src = "../../../cdn-cgi/zaraz/sd0d9.js?z=" + btoa(encodeURIComponent(JSON.stringify(a
                        .zarazData)));
                    t.parentNode.insertBefore(z, t)
                };
                ["complete", "interactive"].includes(e.readyState) ? zaraz.init() : a.addEventListener(
                    "DOMContentLoaded", zaraz.init)
            }(w, d, 0, "script");
        })(window, document);
    </script>
</head>

<body>
    <div class="bg-g1 size1 flex-w flex-col-c-sb p-l-15 p-r-15 p-t-55 p-b-35 respon1">
        <span></span>
        <div class="flex-col-c p-t-50 p-b-50">
            <h3 class="l1-txt1 txt-center p-b-10">
                Coming Soon
            </h3>
            <p class="txt-center l1-txt2 p-b-60">
                Our website is Upgrading
            </p>
            {{-- <div class="flex-w flex-c cd100 p-b-82">
                <div class="flex-col-c-m size2 how-countdown">
                    <span class="l1-txt3 p-b-9 days">35</span>
                    <span class="s1-txt1">Days</span>
                </div>
                <div class="flex-col-c-m size2 how-countdown">
                    <span class="l1-txt3 p-b-9 hours">17</span>
                    <span class="s1-txt1">Hours</span>
                </div>
                <div class="flex-col-c-m size2 how-countdown">
                    <span class="l1-txt3 p-b-9 minutes">50</span>
                    <span class="s1-txt1">Minutes</span>
                </div>
                <div class="flex-col-c-m size2 how-countdown">
                    <span class="l1-txt3 p-b-9 seconds">39</span>
                    <span class="s1-txt1">Seconds</span>
                </div>
            </div> --}}
            <div class="flex-c-m s1-txt2 size3 how-btn">
                Follow us for More Updates!

            </div>
            <div class="d-block folowLInks">

                <a href="https://www.youtube.com/c/TheEducationalNetwork" target="_blank">
                    <img class="yt_red" style="" src="error/images/yt_red.png" alt="">
                </a>

                <a href="https://www.facebook.com/theeducationalwork/" target="_blank"><img class="fb_blue"
                        src="error/images/fb_blue.png" style="" alt=""></a>

                <a href="https://www.instagram.com/theeducationalnetwork/" target="_blank"><img class="insta_color"
                        src="error/images/insta_color.png" style="" alt=""></a>
            </div>


        </div>
        <span class="s1-txt3 txt-center">

        </span>
    </div>



    <script src="error/vendor/jquery/jquery-3.2.1.min.js"></script>

    <script src="error/vendor/bootstrap/js/popper.js"></script>
    <script src="error/vendor/bootstrap/js/bootstrap.min.js"></script>

    <script src="error/vendor/select2/select2.min.js"></script>

    <script src="error/vendor/countdowntime/moment.min.js"></script>
    <script src="error/vendor/countdowntime/moment-timezone.min.js"></script>
    <script src="error/vendor/countdowntime/moment-timezone-with-data.min.js"></script>
    <script src="error/vendor/countdowntime/countdowntime.js"></script>
    <script>
        $('.cd100').countdown100({
            // Set Endtime here
            // Endtime must be > current time
            endtimeYear: 0,
            endtimeMonth: 0,
            endtimeDate: 35,
            endtimeHours: 18,
            endtimeMinutes: 0,
            endtimeSeconds: 0,
            timeZone: ""
            // ex:  timeZone: "America/New_York", can be empty
            // go to " http://momentjs.com/timezone/ " to get timezone
        });
        $('')
    </script>

    <script src="error/vendor/tilt/tilt.jquery.min.js"></script>
    <script>
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>

    <script src="error/js/main.js"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
    </script>
    <script defer src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194"
        integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw=="
        data-cf-beacon='{"rayId":"7498218b0b11c908","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2022.8.1","si":100}'
        crossorigin="anonymous"></script>
</body>

<!-- Mirrored from colorlib.com/etc/cs/comingsoon_04/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Sep 2022 10:59:03 GMT -->

</html>
