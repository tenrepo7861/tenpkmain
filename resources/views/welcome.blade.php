<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/main.css">


</head>

<body class="template-index">
    {{-- <div class="header-img" style="padding:15px;text-align:center">
        <img src="img/logotrans1.png">

    </div> --}}
    <div class="main-content">

        <div class="shopify-section home-index">
            <img src="img/desktop.png" alt="">
            <section class="home-index home-index-desktop">

                <img class="tenlogo" src="img/tenlogo.png" alt="">

                <div class="main-banner" id="home-index-4-slider">

                    <a href="/pages/tentech" class="index-link">
                        <div class="index-grid men-content">


                            {{-- <img class="index-image" src="img/photo-1.jpg"> --}}
                            <div class="index-content">
                                <h2 class="ten">Ten Tech</h2>
                                <p>View</p>
                                <div class="index-desc"></div>



                            </div>

                        </div>
                    </a>
                    <a href="/pages/tenacademy" class="index-link">
                        <div class="index-grid women-content">


                            {{-- <img class="index-image" src="img/acedmey.jpg"> --}}
                            <div class="index-content">
                                <h2 class="academy">Ten Academy</h2>
                                <p>View</p>
                                <div class="index-desc"></div>



                            </div>

                        </div>
                    </a>
                    <a href="#" class="index-link">
                        <div class="index-grid kids-content">


                            {{-- <img class="index-image" src="img/youtube.jpg"> --}}
                            <div class="index-content">
                                <h2 class="automation">Ten Automation</h2>
                                <p>View</p>
                                <div class="index-desc"></div>



                            </div>

                        </div>
                    </a>
                </div>


            </section>
        </div>

    </div>
    <div id="shopify-section-home-index-footer" class="shopify-section">

        <section class="home-index-footer" id="slideshow-home-index-footer">
            <div class="footer-content">
                <div class="index-social">
                    <h5>be a part of our social family</h5>


                    <ul class="social-icons">

                        <li class="facebook">
                            <a href="#" title="Facebook" target="_blank">

                                <i class="fa fa-facebook fa-lg" aria-hidden="true"></i>

                            </a>
                        </li>






                        <li class="instagram">
                            <a href="#" title="Instagram" target="_blank">
                                <i class="fa fa-instagram fa-lg" aria-hidden="true"></i>
                            </a>
                        </li>



                        <li class="pinterest">
                            <a href="#" title="Pinterest" target="_blank">
                                <i class="fa fa-pinterest-p fa-lg" aria-hidden="true"></i>
                            </a>
                        </li>



                        <li class="snapchat">
                            <a href="#" title="Snapchat" target="_blank">
                                <i class="fa fa-snapchat-ghost fa-lg" aria-hidden="true"></i>
                            </a>
                        </li>


                        <li class="youtube">
                            <a href="#" title="YouTube" target="_blank">
                                <i class="fa fa-youtube fa-lg" aria-hidden="true"></i>
                            </a>
                        </li>




                        <li class="vimeo">
                            <a href="#" title="LinkedIn" target="_blank">
                                <i class="fa fa-linkedin fa-lg" aria-hidden="true"></i>
                            </a>
                        </li>

                    </ul>

                </div>
                <div class="index-footer-copyright">
                    <h4>© Copyright 2022 by The Educational Network. All Rights Reserved.</h4>
                </div>
            </div>
        </section>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</body>

</html>
