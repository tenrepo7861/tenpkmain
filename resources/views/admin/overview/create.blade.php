@extends('layouts.master')
@section('title', 'Add Overview')
@section('css_link')
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h5>Create Overview</h5>
            <form action="{{ route('admin.overview.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-row mb-5">
                    <div class="col-md-5">
                        <label>Title</label>
                        <select class="form-control" name="course_id">
                            <option selected disabled>--select one option--</option>
                            @foreach ($courses as $course)
                                <option value="{{ $course->id }}"> {{ $course->title }}</option>
                            @endforeach

                        </select>
                        @error('course_id')
                            <div class="alert alert-danger alert-style" style="margin-top: 8px;">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-5">
                    <div class="col-md-5">
                        <label>Description</label>
                        <textarea class="form-control" name="learn[]" aria-label="With textarea" placeholder="What you'r learn"></textarea>
                        @error('learn')
                            <div class="alert alert-danger alert-style">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-3">

                        <button type="button" class="btn btn-primary add-more-learn" id="" style="margin-top: 30px;">Add</button>
                    </div>
                </div>
                <div class="form-row field_wrapper mb-5" style="display: none">

                </div>

                <div class="form-row mb-5">
                    <div class="col-md-5">
                        <label>Requirements</label>
                        <textarea class="form-control" name="requirements[]" aria-label="With textarea" placeholder="Coures Requirements"></textarea>
                        @error('requirements')
                            <div class="alert alert-danger alert-style">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-primary add-more-requirements" id="" style="margin-top: 30px;">Add</button>
                    </div>
                </div>
                <div class="form-row field_wrapper_requirement mb-5" style="display: none"></div>

                <button type="submit" class="btn  btn-primary">Submit</button>

            </form>

        </div>

    </div>
@endsection

@section('js_link')
    <script>
        $(document).ready(function() {
            var max_feild = 50;
            var wrapper = $(".field_wrapper");
            var add_button = $(".add-more-learn");
            var x = 1;
            $(add_button).click(function(e) {
                e.preventDefault();
                $('.field_wrapper').css('display', 'block');
                if (x < max_feild) {
                    $(wrapper).append(
                        '<div class="ggg"><div class="col-md-5"> <h3 style="font-size:12px;">Description No</h3><textarea class="form-control" name="learn[]" aria-label="With textarea">' +
                        '</textarea>' + '</div' +
                        '<div class="col-md-3"> <a style="margin-bottom: 8px;position: absolute;margin-left: 465px;top:0px;" href="JavaScript:Void(0);" class="btn btn-danger remove mt-4">Remove</a></div></div>'
                    );
                    x++;
                }
                if (x == max_feild) {
                    alert("you reached the limit");
                }
            });
            $(wrapper).on("click", ".remove", function(e) {
                e.preventDefault();
                $(this).parent("div").remove();
                x--;
            });



        });
    </script>
    <script>
        //code for requirement dynamic field
        $(document).ready(function() {
            var max_feild = 50;
            var wrapper = $(".field_wrapper_requirement");
            var add_button = $(".add-more-requirements");
            var x = 1;
            $(add_button).click(function(e) {
                e.preventDefault();
                $('.field_wrapper_requirement').css('display', 'block');
                if (x < max_feild) {
                    $(wrapper).append(
                        '<div class="ggg"><div class="col-md-5"> <h3 style="font-size:12px;">Requirements</h3><textarea class="form-control" name="requirements[]" aria-label="With textarea">' +
                        '</textarea>' + '</div' +
                        '<div class="col-md-3"> <a style="margin-bottom: 8px;position: absolute;margin-left: 465px;top:0px;" href="JavaScript:Void(0);" class="btn btn-danger remove_requirements mt-4">Remove</a></div></div>'
                    );
                    x++;
                }
                if (x == max_feild) {
                    alert("you reached the limit");
                }
            });
            $(wrapper).on("click", ".remove_requirements", function(e) {
                e.preventDefault();
                $(this).parent("div").remove();
                x--;
            });



        });
    </script>

    <script src="https://code.iconify.design/iconify-icon/1.0.0-beta.3/iconify-icon.min.js"></script>
@endsection
