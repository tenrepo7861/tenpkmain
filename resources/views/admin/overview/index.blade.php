@extends('layouts.master')
@section('title', 'Overview')
@section('css_link')
@endsection

@section('content')
    <div class="row justify-content-center" id="roleTble">
        <div class="col-md-12">
            <a href="{{ route('admin.overview.create') }}" class="btn  btn-primary" style="margin-bottom: 20px;">Add Overview</a>
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                    <span class="alert-inner--text"><strong>Success!</strong> {{ Session::get('success') }}</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card mt-3">
                <div class="card-header">
                    <h5 class="text-center">Avaible Overviews</h5>
                </div>
                <div class="card-body table-border-style">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Duration</th>
                                    <th>Start Date</th>
                                    <th colspan="2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($overviews->isNotEmpty())
                                    @foreach ($overviews as $overview)
                                        <tr>
                                            <td>#{{ $overview->id }}</td>
                                            <td>{{$overview->course->title}}</td>
                                            <td>{{$overview->learn}}</td>
                                            <td>sdfdsf</td>
                                            <td>fsdf</td>
                                            <td>fsdf</td>
                                            <td>fdsdf</td>
                                            <td>
                                                <a class="btn btn-primary btn-sm" id="edit">Edit</a>
                                                <a class="btn  btn-danger btn-danger" id="delete">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="text-center text-danger">
                                            <p>No Overviews founds</p>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('js_link')
@endsection
