@extends('layouts.master')
@section('title', 'Courses')
@section('css_link')
<style>
    #delete {
    height: 28px;
    font-size: 10px;
    line-height: 5px;
    color: white;
}
  #edit {
    height: 28px;
    font-size: 12px;
    line-height: 15px;
    color: white;
}
</style>
@endsection

@section('content')
    <div class="row justify-content-center" id="roleTble">
        <div class="col-md-12">
            <a href="{{ route('admin.course.create') }}" class="btn  btn-primary" style="margin-bottom: 20px;">Add Course</a>
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                    <span class="alert-inner--text"><strong>Success!</strong> {{ Session::get('success') }}</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card mt-3">
                <div class="card-header">
                    <h5 class="text-center">Avaible Courses</h5>
                </div>
                <div class="card-body table-border-style">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Duration</th>
                                    <th>Start Date</th>
                                    <th colspan="2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($courses->isNotEmpty())
                                    @foreach ($courses as $course)
                                        <tr>
                                            <td>#{{ $course->id }}</td>
                                            <td>
                                                @if ($course->image != '')
                                                    <img src="{{ asset('courseImages/' . $course->image) }}" class="img-fluid rounded" alt="error" width="70" height="60" />
                                            </td>
                                        @else
                                            <p class="text-danger text-center">No image</p>
                                    @endif
                                    </td>
                                    <td>{{ $course->title }}</td>
                                    <td>{{ $course->description }}</td>
                                    <td>{{ $course->price }}</td>
                                    <td>{{ $course->duration }}</td>
                                    <td>{{ date('d-m-Y', strtotime($course->created_at)) }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"  id="edit">Edit</a>
                                        <a class="btn  btn-danger btn-danger" id="delete">Delete</a>
                                    </td>
                                    </tr>
                                @endforeach
                            @else
                                <p>No posts found</p>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js_link')
@endsection
