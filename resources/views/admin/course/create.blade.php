@extends('layouts.master')
@section('title', 'Create Courses')
@section('css_link')
    <style>
        .alert-style {
            color: #78281f;
            background-color: #fadbd8;
            border-color: #f8cdc8;
            margin-top: 9px;
            margin-bottom: -26px;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h5>Create Course</h5>
            <form action="{{ route('admin.course.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="form-row mb-5">
                    <div class="col">
                        <label>Title</label>
                        <input type="text" name="title" value="{{ old('title') }}" class="form-control" placeholder="Title">
                        @error('title')
                            <div class="alert alert-danger alert-style">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col">
                        <label>Description</label>
                        <input type="text" name="description" value="{{ old('description') }}" class="form-control" placeholder="Description">
                        @error('description')
                            <div class="alert alert-danger alert-style">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-5">
                    <div class="col">
                        <label>Price</label>
                        <input type="number" name="price" value="{{ old('price') }}" class="form-control">
                        @error('price')
                            <div class="alert alert-danger alert-style">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col">
                        <label>Duration</label>
                        <input type="text" name="duration" value="{{ old('duration') }}" class="form-control" placeholder="Duration">
                        @error('duration')
                            <div class="alert alert-danger alert-style">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-5">
                    <div class="col">
                        <label>Upload File</label>
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" id="inputGroupFile02">
                            <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                        </div>
                        @error('image')
                            <div class="alert alert-danger alert-style">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col">
                        <label>Course Description 1</label>
                        <textarea class="form-control" name="description_include[]" aria-label="With textarea"></textarea>
                    </div>
                    <div class="col">

                        <button type="button" class="btn btn-primary add-more" id="addMore" style="margin-top: 40px;">Add</button>
                    </div>
                </div>
                 <div class="col-6 field_wrapper mb-5">
                </div>

                <div class="form-row mb-2">
                    <div class="col-md-8">
                        <label>Course Description</label>
                        <textarea class="form-control ckeditor" name="about_course" aria-label="With textarea"></textarea>
                    </div>
                </div>


                <button type="submit" class="btn  btn-primary">Submit</button>



            </form>

        </div>

    </div>
@endsection
@section('js_link')
    <script>
        $(document).ready(function() {
            var max_feild = 50;
            var wrapper = $(".field_wrapper");
            var add_button = $(".add-more");
            var x = 1;
            var count = 1;
            $(add_button).click(function(e) {
                e.preventDefault();
                var desc_no = count++;
                if (x < max_feild) {
                    $(wrapper).append(
                        '<div><h3 style="font-size:12px;">Description No  <span id="descs_no' + desc_no +
                        '"></span></h3><textarea class="form-control" name="description_include[]" aria-label="With textarea">' +
                        '</textarea>' +
                        '<a style="margin-bottom: 8px;position: absolute;margin-left: 460px;" href="JavaScript:Void(0);" class="btn btn-danger remove mt-4">Remove</a></div>'
                    );

                    $("#descs_no" + desc_no).text(count);
                    x++;
                }
                if (x == max_feild) {
                    alert("you reached the limit");
                }
            });
            $(wrapper).on("click", ".remove", function(e) {
                e.preventDefault();
                $(this).parent("div").remove();
                $("#descs_no" + desc_no).text(count);
                x--;
            });



        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
    </script>
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script src="https://code.iconify.design/iconify-icon/1.0.0-beta.3/iconify-icon.min.js"></script>
@endsection
