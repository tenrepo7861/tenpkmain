@extends('layouts.master')
@section('title', 'Create Courses')
@section('css_link')
@endsection

@section('content')
    <h1>Roles</h1>

    <h3>Create Roles</h3>
    <form action="/roles" method="post">
        @csrf
        <input type="text" name="role">
        <input type="text" name="permission">
        <button type="submit">Create Role</button>
    </form>
@endsection
