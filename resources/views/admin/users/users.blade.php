@extends('layouts.master')
@section('title', 'Create Courses')
@section('css_link')
@endsection

@section('content')


    <h1>Users</h1>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>


                <th>Assign Role</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user as $row)
                <tr>
                    <th scope="row">{{ $row->id }}</th>
                    <td>{{ $row->first_name }}</td>
                    <td>{{ $row->last_name }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->phone }}</td>

                    <td>
                        <button type="button" class="btn btn-primary opnModal" data-bs-toggle="modal"
                            data-id="{{ $row->id }}" data-bs-target="#exampleModal">
                            Assign Role
                        </button>
                        {{-- <select class="form-control" name="assignRole" id="assignRole">
                            @foreach ($role as $r)
                                <option value="{{ $r->id }}">{{ $r->name }}
                                </option>
                            @endforeach
                        </select> --}}

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('assignRole') }}" method="post">
                        @csrf
                        <input type="hidden" name="userId" id="userId" value="" />

                        {{-- <input type="text" name="assignRole" id="assignRole"> --}}
                        <select class="form-control" name="assignRole" id="assignRole">
                            @foreach ($role as $r)
                                <option value="{{ $r->id }}">{{ $r->name }}</option>
                            @endforeach

                        </select>
                        {{-- <button type="submit">Assign Role</button> --}}


                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Assign Role</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"
    integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        $('.opnModal').on('click', function() {
            var user_id = $(this).attr('data-id');
            // alert(user_id);
            $("#userId").val(user_id);
        });
    });
</script>
{{-- @section('js_link')
    <script>
        $(document).ready(function() {

            $('#roles').click(function() {

                alert("you reached the limit");

            });



        });
    </script>
    <script src="https://code.iconify.design/iconify-icon/1.0.0-beta.3/iconify-icon.min.js"></script>
@endsection --}}
