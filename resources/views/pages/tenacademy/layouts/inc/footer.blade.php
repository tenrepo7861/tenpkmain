 <!-- Call To Action Section Two -->
 <section class="call-to-action-section-two" style="background-image: url(/tenacademy/images/background/3.png)">
     <div class="auto-container">
         <div class="content">
             <h2>Explore Courses</h2>
             <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two
                 <br> waters own morning gathered greater shall had behold had seed.
             </div>
             <div class="buttons-box">
                 {{-- <a href="{{ route('course.list') }}" class="theme-btn btn-style-one"><span class="txt">Get Stared <i
                             class="fa fa-angle-right"></i></span></a> --}}
                 <a href="{{ route('course.list') }}" class="theme-btn btn-style-two"><span class="txt">All Courses
                         <i class="fa fa-angle-right"></i></span></a>
             </div>
         </div>
     </div>
 </section>
 <!-- End Call To Action Section Two -->
 <footer class="main-footer">
     <!-- Pattern Layer -->
     <div class="pattern-layer paroller" data-paroller-factor="0.60" data-paroller-factor-lg="0.20"
         data-paroller-type="foreground" data-paroller-direction="vertical"
         style="background-image:url(/tenacademy/images/icons/icon-1.png)"></div>
     <div class="pattern-layer-two data-paroller-factor="0.60" data-paroller-factor-lg="0.20"
         data-paroller-type="foreground" data-paroller-direction="vertical"
         style="background-image:url(/tenacademy/images/icons/icon-3.png)"></div>
     <div class="auto-container">

         <!-- Widgets Section -->
         <div class="widgets-section">
             <div class="row clearfix">

                 <!-- Big Column -->
                 <div class="big-column col-lg-7 col-md-12 col-sm-12">
                     <div class="row clearfix">

                         <!--Footer Column-->
                         <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                             <div class="footer-widget logo-widget">
                                 <div class="logo">
                                     <a href="index.html"><img src="/tenacademy/images/tenacademy2.png" alt=""
                                             style="height:150px" /></a>
                                 </div>
                                 {{-- <div class="text">Replenish him third creature and meat blessed void a fruit gathered
                                     you’re, they’re two waters own morning gathered greater.</div> --}}

                             </div>
                         </div>

                         <!--Footer Column-->
                         <div class="footer-column col-lg-4 col-md-4 col-sm-12">
                             <div class="footer-widget links-widget">
                                 <h4>Quick Links</h4>
                                 <ul class="links-widget">

                                     <li><a href="{{ route('home') }}">Home</a></li>
                                     <li><a href="{{ route('price') }}">Pricing</a></li>
                                     <li><a href="{{ route('contact-us') }}">Contact</a></li>
                                     <li><a href="#">Privacy Policy</a></li>
                                 </ul>


                             </div>

                         </div>
                         <div class="footer-column col-lg-2 col-md-2 col-sm-12">
                             <div class="footer-widget links-widget">
                                 <h4 style="visibility: hidden">Resource</h4>
                                 <ul class="links-widget">
                                     <li><a href="#">Reviews</a></li>
                                     <li><a href="#">About Us</a></li>
                                     <li><a href="#">Blogs</a></li>
                                     <li><a href="#">FAQs</a></li>
                                     {{-- <li><a href="#">Terms of Policy</a></li> --}}
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>

                 <!-- Big Column -->
                 <div class="big-column col-lg-5 col-md-12 col-sm-12">
                     <div class="row clearfix">

                         <!--Footer Column-->


                         <!--Footer Column-->
                         <div class="footer-column col-lg-7 col-md-7 col-sm-12">
                             <div class="footer-widget links-widget">
                                 <h4>Contact us</h4>
                                 <ul class="links-widget">
                                     <li> <i class="fa fa-phone"></i> Phone: +92 336 9864024</li>
                                     <li><i class="fa fa-envelope"></i> Email: info@tenacademy.com</li>
                                 </ul>
                             </div>
                         </div>
                         <div class="footer-column col-lg-5 col-md-5 col-sm-12">
                             <div class="footer-widget links-widget">
                                 <h4>Follow Us</h4>
                                 <ul class="logo-widget">
                                     <div class="social-box">
                                         <a href="#" class="fa fa-facebook"></a>
                                         <a href="#" class="fa fa-instagram"></a>
                                         <a href="#" class="fa fa-twitter"></a>
                                         <a href="#" class="fa fa-google"></a>
                                     </div>
                                 </ul>
                             </div>
                         </div>

                     </div>
                 </div>

             </div>
         </div>

     </div>
     <div style="text-align: center;background-color:black;height:45px">
         <div class="copyright" style="font-size: 20px;padding-top:15px">Copyright &copy; 2020 The Educational Network
         </div>

     </div>
 </footer>
